#include "keymap.h"
#include "keyboard.h"
#include "action.h"
#include "matrix.h"
#include "suspend.h"
#include "bootloader.h"
#include "unimap_trans.h"
#if defined(__AVR__)
#   include <avr/pgmspace.h>
#endif

#include "custom_util.h"
#include "custom_led.h"


enum function_id {
    FUNC_LED_SWITCH,
    FUNC_LED_ROTATE_HUE,
    FUNC_LED_ROTATE_MIN_BRIGHTNESS,
    FUNC_RESET,
};

enum layer_id {
    LAYER_SAFETY = 0,
    LAYER_BMS_F16_STICK,
    LAYER_BMS_F16_TQS,
    LAYER_BMS_F16_ICP,
    LAYER_BMS_F16_LMFD,
    LAYER_BMS_F16_RMFD,
    LAYER_BMS_F16_PANELS,
    
    LAYER_COUNT,
};

#define AC_LEDSW       ACTION_FUNCTION_TAP(FUNC_LED_SWITCH)
#define AC_LEDHUE      ACTION_FUNCTION_TAP(FUNC_LED_ROTATE_HUE)
#define AC_LEDBRT      ACTION_FUNCTION_TAP(FUNC_LED_ROTATE_MIN_BRIGHTNESS)

#define AC_RESET       ACTION_FUNCTION_TAP(FUNC_RESET)

#define AC_OPENTRACK_CENTER     ACTION_MODS_KEY(MOD_LCTL | MOD_LALT | MOD_LSFT, KC_F12)
#define AC_OPENTRACK_TOGGLE     ACTION_MODS_KEY(MOD_LCTL | MOD_LALT | MOD_LSFT, KC_F11)

/********** BMS keys **********/

#define AC_LY_BMS_F16_STICK     ACTION_LAYER_TOGGLE(LAYER_BMS_F16_STICK)
#define AC_LY_BMS_F16_TQS       ACTION_LAYER_TOGGLE(LAYER_BMS_F16_TQS)
#define AC_LY_BMS_F16_ICP       ACTION_LAYER_TOGGLE(LAYER_BMS_F16_ICP)
#define AC_LY_BMS_F16_LMFD      ACTION_LAYER_TOGGLE(LAYER_BMS_F16_LMFD)
#define AC_LY_BMS_F16_RMFD      ACTION_LAYER_TOGGLE(LAYER_BMS_F16_RMFD)
#define AC_LY_BMS_F16_PANELS    ACTION_LAYER_TOGGLE(LAYER_BMS_F16_PANELS)

/**** Stick ****/

/** things originally mapped to a controller **/

#define AC_BMS_F16_TMS_UP              ACTION_MODS_KEY(MOD_LSFT, KC_HOME)
#define AC_BMS_F16_TMS_DOWN            ACTION_MODS_KEY(MOD_LSFT, KC_END)
#define AC_BMS_F16_TMS_LEFT            ACTION_MODS_KEY(MOD_LSFT, KC_DEL)
#define AC_BMS_F16_TMS_RIGHT           ACTION_MODS_KEY(MOD_LSFT, KC_PGDN)

#define AC_BMS_F16_TRIM_UP             ACTION_MODS_KEY(MOD_LCTL, KC_UP)
#define AC_BMS_F16_TRIM_DOWN           ACTION_MODS_KEY(MOD_LCTL, KC_DOWN)
#define AC_BMS_F16_TRIM_LEFT           ACTION_MODS_KEY(MOD_LCTL, KC_LEFT)
#define AC_BMS_F16_TRIM_RIGHT          ACTION_MODS_KEY(MOD_LCTL, KC_RGHT)
#define AC_BMS_F16_TRIM_RESET          ACTION_MODS_KEY(MOD_LALT, KC_F1)

#define AC_BMS_F16_1ST_TRIGGER_DETENT  ACTION_MODS_KEY(MOD_LCTL, KC_SLSH)
#define AC_BMS_F16_2ND_TRIGGER_DETENT  ACTION_MODS_KEY(MOD_LALT, KC_SLSH)
#define AC_BMS_F16_WPN_REL             ACTION_KEY(KC_SPC)

#define AC_BMS_F16_NWS_AR_MSL          ACTION_MODS_KEY(MOD_LSFT, KC_SLSH)
#define AC_BMS_F16_PINKY_SWITCH        ACTION_KEY(KC_V)
#define AC_BMS_F16_PADDLE_SWITCH       ACTION_MODS_KEY(MOD_LALT, KC_A)

/****/

#define AC_BMS_F16_DMS_UP       ACTION_MODS_KEY(MOD_LCTL, KC_HOME)
#define AC_BMS_F16_DMS_DOWN     ACTION_MODS_KEY(MOD_LCTL, KC_END)
#define AC_BMS_F16_DMS_LEFT     ACTION_MODS_KEY(MOD_LCTL, KC_DEL)
#define AC_BMS_F16_DMS_RIGHT    ACTION_MODS_KEY(MOD_LCTL, KC_PGDN)

#define AC_BMS_F16_CMS_UP       ACTION_MODS_KEY(MOD_LALT, KC_HOME)
#define AC_BMS_F16_CMS_DOWN     ACTION_MODS_KEY(MOD_LALT, KC_END)
#define AC_BMS_F16_CMS_LEFT     ACTION_MODS_KEY(MOD_LALT, KC_DEL)
#define AC_BMS_F16_CMS_RIGHT    ACTION_MODS_KEY(MOD_LALT, KC_PGDN)

/**** TQS ****/

#define AC_BMS_F16_DF_OVERRIDE_OR_TQS_EN      ACTION_LAYER_TAP_KEY(LAYER_BMS_F16_TQS, KC_D)    // XXX: Hold to access TQS buttons
#define AC_BMS_F16_MRM_OVERRIDE_OR_PANELS_EN  ACTION_LAYER_TAP_KEY(LAYER_BMS_F16_PANELS, KC_M) // XXX: Hold to access panel buttons
#define AC_BMS_F16_DF_CANCEL                  ACTION_KEY(KC_C)

#define AC_BMS_F16_CURSOR_UP     ACTION_MODS_KEY(MOD_LSFT, KC_UP)
#define AC_BMS_F16_CURSOR_DOWN   ACTION_MODS_KEY(MOD_LSFT, KC_DOWN)
#define AC_BMS_F16_CURSOR_LEFT   ACTION_MODS_KEY(MOD_LSFT, KC_LEFT)
#define AC_BMS_F16_CURSOR_RIGHT  ACTION_MODS_KEY(MOD_LSFT, KC_RGHT)
#define AC_BMS_F16_CURSOR_ENABLE ACTION_KEY(KC_INS)
#define AC_BMS_F16_CURSOR_ZERO   ACTION_KEY(KC_PGUP)

#define AC_BMS_F16_RANGE_UP     ACTION_MODS_KEY(MOD_LALT, KC_F4)
#define AC_BMS_F16_RANGE_DOWN   ACTION_MODS_KEY(MOD_LALT, KC_F3)
#define AC_BMS_F16_UNCAGE       ACTION_KEY(KC_U)

#define AC_BMS_F16_ANT_UP       ACTION_MODS_KEY(MOD_LCTL, KC_F7)
#define AC_BMS_F16_ANT_CENTER   ACTION_MODS_KEY(MOD_LCTL, KC_F6)
#define AC_BMS_F16_ANT_DOWN     ACTION_MODS_KEY(MOD_LCTL, KC_F5)

#define AC_BMS_F16_SPD_BRK_TOGGLE ACTION_KEY(KC_B)
#define AC_BMS_F16_SPD_BRK_OPEN   ACTION_MODS_KEY(MOD_LSFT, KC_B)
#define AC_BMS_F16_SPD_BRK_CLOSE  ACTION_MODS_KEY(MOD_LCTL, KC_B)

#define AC_BMS_F16_UHF          ACTION_KEY(KC_HOME)
#define AC_BMS_F16_VHF          ACTION_KEY(KC_END)
#define AC_BMS_F16_IFF_OUT      ACTION_KEY(KC_DEL)
#define AC_BMS_F16_IFF_IN       ACTION_KEY(KC_PGDN)

#define AC_BMS_F16_IDLE_DETENT_TOGGLE  ACTION_MODS_KEY(MOD_LALT, KC_I)

/**** Panels ****/

#define AC_BMS_F16_CMS_SLAP_BTN        ACTION_KEY(KC_S)

#define AC_BMS_F16_LANDING_GEAR_TOGGLE ACTION_KEY(KC_G)
#define AC_BMS_F16_EMER_JETT_BTN       ACTION_MODS_KEY(MOD_LCTL, KC_J)
#define AC_BMS_F16_PRK_BRK_CYCLE       ACTION_MODS_KEY(MOD_LALT, KC_P)

#define AC_BMS_F16_MASTER_ARM_CYCLE    ACTION_MODS_KEY(MOD_LSFT, KC_M)

#define AC_BMS_F16_MAN_PITCH_TOGGLE    ACTION_KEY(KC_O)

#define AC_BMS_F16_MAIN_PWR_UP         ACTION_MODS_KEY(MOD_LALT | MOD_LSFT, KC_W)
#define AC_BMS_F16_MAIN_PWR_DOWN       ACTION_MODS_KEY(MOD_LALT | MOD_LSFT, KC_Q)

#define AC_BMS_F16_EJECT               ACTION_MODS_KEY(MOD_LCTL, KC_E)

#define AC_BMS_F16_NIGHTVISION_TOGGLE  ACTION_KEY(KC_N)
#define AC_BMS_F16_SPOTLIGHT_TOGGLE    ACTION_MODS_KEY(MOD_LSFT, KC_S)

#define AC_BMS_F16_AVTR_CYCLE          ACTION_MODS_KEY(MOD_LALT, KC_F)
#define AC_BMS_F16_AVTR_TOGGLE         ACTION_KEY(KC_F)

/**** ICP ****/

#define AC_BMS_F16_ICP_COM1            ACTION_KEY(KC_F1)
#define AC_BMS_F16_ICP_COM2            ACTION_KEY(KC_F2)
#define AC_BMS_F16_ICP_IFF             ACTION_KEY(KC_F3)
#define AC_BMS_F16_ICP_LIST            ACTION_KEY(KC_F4)
#define AC_BMS_F16_ICP_AA              ACTION_KEY(KC_F5)
#define AC_BMS_F16_ICP_AG              ACTION_KEY(KC_F6)
#define AC_BMS_F16_ICP_NAV             ACTION_MODS_KEY(MOD_LSFT, KC_BSPC)
#define AC_BMS_F16_ICP_1               ACTION_KEY(KC_P1)
#define AC_BMS_F16_ICP_2               ACTION_KEY(KC_P2)
#define AC_BMS_F16_ICP_3               ACTION_KEY(KC_P3)
#define AC_BMS_F16_ICP_4               ACTION_KEY(KC_P4)
#define AC_BMS_F16_ICP_5               ACTION_KEY(KC_P5)
#define AC_BMS_F16_ICP_6               ACTION_KEY(KC_P6)
#define AC_BMS_F16_ICP_7               ACTION_KEY(KC_P7)
#define AC_BMS_F16_ICP_8               ACTION_KEY(KC_P8)
#define AC_BMS_F16_ICP_9               ACTION_KEY(KC_P9)
#define AC_BMS_F16_ICP_0               ACTION_KEY(KC_P0)
#define AC_BMS_F16_ICP_RCL             ACTION_KEY(KC_PDOT)
#define AC_BMS_F16_ICP_ENTER           ACTION_KEY(KC_PENT)
#define AC_BMS_F16_ICP_NEXT            ACTION_KEY(KC_PPLS)
#define AC_BMS_F16_ICP_PREV            ACTION_KEY(KC_PMNS)
#define AC_BMS_F16_ICP_DCS_UP          ACTION_KEY(KC_UP)
#define AC_BMS_F16_ICP_DCS_DOWN        ACTION_KEY(KC_DOWN)
#define AC_BMS_F16_ICP_DCS_SEQ         ACTION_KEY(KC_RGHT)
#define AC_BMS_F16_ICP_DCS_RTN         ACTION_KEY(KC_LEFT)
#define AC_BMS_F16_ICP_DRIFT_CO_TOGGLE ACTION_KEY(KC_PSLS)

/**** LMFD ****/

#define AC_BMS_F16_LMFD_OSB_1          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_1)
#define AC_BMS_F16_LMFD_OSB_2          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_2)
#define AC_BMS_F16_LMFD_OSB_3          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_3)
#define AC_BMS_F16_LMFD_OSB_4          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_4)
#define AC_BMS_F16_LMFD_OSB_5          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_5)
#define AC_BMS_F16_LMFD_OSB_6          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_6)
#define AC_BMS_F16_LMFD_OSB_7          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_7)
#define AC_BMS_F16_LMFD_OSB_8          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_8)
#define AC_BMS_F16_LMFD_OSB_9          ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_9)
#define AC_BMS_F16_LMFD_OSB_10         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_0)
#define AC_BMS_F16_LMFD_OSB_11         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P1)
#define AC_BMS_F16_LMFD_OSB_12         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P2)
#define AC_BMS_F16_LMFD_OSB_13         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P3)
#define AC_BMS_F16_LMFD_OSB_14         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P4)
#define AC_BMS_F16_LMFD_OSB_15         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P5)
#define AC_BMS_F16_LMFD_OSB_16         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P6)
#define AC_BMS_F16_LMFD_OSB_17         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P7)
#define AC_BMS_F16_LMFD_OSB_18         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P8)
#define AC_BMS_F16_LMFD_OSB_19         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P9)
#define AC_BMS_F16_LMFD_OSB_20         ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_P0)

#define AC_BMS_F16_LMFD_INC_BRHT       ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_EQL)
#define AC_BMS_F16_LMFD_DEC_BRHT       ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_MINS)

#define AC_BMS_F16_LMFD_INC_GAIN       ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_GRV)
#define AC_BMS_F16_LMFD_DEC_GAIN       ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_GRV)

/**** RMFD ****/

#define AC_BMS_F16_RMFD_OSB_1          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_1)
#define AC_BMS_F16_RMFD_OSB_2          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_2)
#define AC_BMS_F16_RMFD_OSB_3          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_3)
#define AC_BMS_F16_RMFD_OSB_4          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_4)
#define AC_BMS_F16_RMFD_OSB_5          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_5)
#define AC_BMS_F16_RMFD_OSB_6          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_6)
#define AC_BMS_F16_RMFD_OSB_7          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_7)
#define AC_BMS_F16_RMFD_OSB_8          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_8)
#define AC_BMS_F16_RMFD_OSB_9          ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_9)
#define AC_BMS_F16_RMFD_OSB_10         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_0)
#define AC_BMS_F16_RMFD_OSB_11         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P1)
#define AC_BMS_F16_RMFD_OSB_12         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P2)
#define AC_BMS_F16_RMFD_OSB_13         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P3)
#define AC_BMS_F16_RMFD_OSB_14         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P4)
#define AC_BMS_F16_RMFD_OSB_15         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P5)
#define AC_BMS_F16_RMFD_OSB_16         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P6)
#define AC_BMS_F16_RMFD_OSB_17         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P7)
#define AC_BMS_F16_RMFD_OSB_18         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P8)
#define AC_BMS_F16_RMFD_OSB_19         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P9)
#define AC_BMS_F16_RMFD_OSB_20         ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_P0)

#define AC_BMS_F16_RMFD_INC_BRHT       ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_EQL)
#define AC_BMS_F16_RMFD_DEC_BRHT       ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_MINS)

#define AC_BMS_F16_RMFD_INC_GAIN       ACTION_MODS_KEY(MOD_LSFT | MOD_LALT, KC_GRV) // The same as LMFD
#define AC_BMS_F16_RMFD_DEC_GAIN       ACTION_MODS_KEY(MOD_LCTL | MOD_LALT, KC_GRV) // The same as LMFD

/**** Misc. ****/

#define AC_BMS_F16_WHEEL_BRAKES        ACTION_KEY(KC_K)

/********** BMS keys end **********/

#ifndef _
#   define _ TRNS
#else
#   error "The _ macro is already defined!"
#endif

#ifdef KEYMAP_SECTION_ENABLE
const action_t actionmaps[][UNIMAP_ROWS][UNIMAP_COLS] __attribute__ ((section (".keymap.keymaps"))) = {
#else
const action_t actionmaps[][UNIMAP_ROWS][UNIMAP_COLS] PROGMEM = {
#endif

    [LAYER_SAFETY] = UNIMAP_TANUKI(
        NO, OPENTRACK_CENTER, OPENTRACK_TOGGLE, NO, NO, NO, NO, NO, NO, LEDSW, LEDHUE, LEDBRT,
        LY_BMS_F16_STICK, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO,
          NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO,
             NO, NO, NO,  RESET,     NO,   NO, NO),

    [LAYER_BMS_F16_STICK] = UNIMAP_TANUKI(
        NO, BMS_F16_TRIM_RESET, BMS_F16_TRIM_UP, BMS_F16_PADDLE_SWITCH, BMS_F16_PINKY_SWITCH, BMS_F16_DMS_LEFT, BMS_F16_DMS_UP, BMS_F16_DMS_DOWN, BMS_F16_DMS_RIGHT, LY_BMS_F16_LMFD, LY_BMS_F16_RMFD, LY_BMS_F16_ICP,
        LY_BMS_F16_STICK, BMS_F16_TRIM_LEFT, BMS_F16_TRIM_DOWN, BMS_F16_TRIM_RIGHT, BMS_F16_1ST_TRIGGER_DETENT, BMS_F16_TMS_LEFT, BMS_F16_TMS_UP, BMS_F16_TMS_DOWN, BMS_F16_TMS_RIGHT, BMS_F16_2ND_TRIGGER_DETENT, NO, NO,
          NO, NO, NO, BMS_F16_CMS_SLAP_BTN, BMS_F16_CMS_LEFT, BMS_F16_CMS_UP, BMS_F16_CMS_DOWN, BMS_F16_CMS_RIGHT, NO, NO, NO,
             NO, NO, BMS_F16_NWS_AR_MSL,  BMS_F16_WPN_REL,     BMS_F16_DF_OVERRIDE_OR_TQS_EN,   BMS_F16_MRM_OVERRIDE_OR_PANELS_EN, BMS_F16_DF_CANCEL),

    [LAYER_BMS_F16_TQS] = UNIMAP_TANUKI(
        BMS_F16_IDLE_DETENT_TOGGLE, BMS_F16_IFF_OUT, BMS_F16_IFF_IN, NO, BMS_F16_SPD_BRK_CLOSE, BMS_F16_UNCAGE, BMS_F16_RANGE_UP, BMS_F16_RANGE_DOWN, NO, NO, NO, NO,
        NO, BMS_F16_UHF, NO, BMS_F16_SPD_BRK_TOGGLE, BMS_F16_CURSOR_ZERO, BMS_F16_CURSOR_LEFT, BMS_F16_CURSOR_UP, BMS_F16_CURSOR_DOWN, BMS_F16_CURSOR_RIGHT, BMS_F16_CURSOR_ENABLE, NO, NO,
         NO, BMS_F16_VHF, NO, BMS_F16_SPD_BRK_OPEN, BMS_F16_ANT_CENTER, BMS_F16_ANT_UP, BMS_F16_ANT_DOWN, NO, NO, NO, NO,
          NO, NO, NO, NO, BMS_F16_DF_OVERRIDE_OR_TQS_EN, NO, NO),

    [LAYER_BMS_F16_ICP] = UNIMAP_TANUKI(
        NO, BMS_F16_ICP_DCS_UP, NO, BMS_F16_ICP_NEXT, NO, BMS_F16_ICP_3, BMS_F16_ICP_2, BMS_F16_ICP_1, NO, NO, NO, LY_BMS_F16_ICP,
        BMS_F16_ICP_DCS_RTN, BMS_F16_ICP_DCS_DOWN, BMS_F16_ICP_DCS_SEQ, BMS_F16_ICP_ENTER, BMS_F16_ICP_0, BMS_F16_ICP_6, BMS_F16_ICP_5, BMS_F16_ICP_4, BMS_F16_ICP_RCL, BMS_F16_ICP_DRIFT_CO_TOGGLE, NO, NO,
          NO, NO, BMS_F16_ICP_PREV, NO, BMS_F16_ICP_9, BMS_F16_ICP_8, BMS_F16_ICP_7, NO, NO, NO, NO,
           BMS_F16_ICP_COM1, BMS_F16_ICP_COM2, BMS_F16_ICP_IFF, BMS_F16_ICP_LIST, BMS_F16_ICP_AA, BMS_F16_ICP_AG, BMS_F16_ICP_NAV),

    [LAYER_BMS_F16_LMFD] = UNIMAP_TANUKI(
        BMS_F16_LMFD_OSB_20, NO, NO, NO, BMS_F16_LMFD_OSB_1, BMS_F16_LMFD_OSB_2, BMS_F16_LMFD_OSB_3, BMS_F16_LMFD_OSB_4, BMS_F16_LMFD_OSB_5, LY_BMS_F16_LMFD, NO, BMS_F16_LMFD_OSB_6,
        BMS_F16_LMFD_OSB_19, BMS_F16_LMFD_INC_BRHT, BMS_F16_LMFD_INC_GAIN, NO, NO, NO, NO, NO, NO, NO, NO, BMS_F16_LMFD_OSB_7,
          BMS_F16_LMFD_OSB_18, BMS_F16_LMFD_DEC_BRHT, BMS_F16_LMFD_DEC_GAIN, BMS_F16_LMFD_OSB_15, BMS_F16_LMFD_OSB_14, BMS_F16_LMFD_OSB_13, BMS_F16_LMFD_OSB_12, BMS_F16_LMFD_OSB_11, NO, NO, BMS_F16_LMFD_OSB_8,
            BMS_F16_LMFD_OSB_17, BMS_F16_LMFD_OSB_16, NO, NO, NO, BMS_F16_LMFD_OSB_10, BMS_F16_LMFD_OSB_9),

    [LAYER_BMS_F16_RMFD] = UNIMAP_TANUKI(
        BMS_F16_RMFD_OSB_20, NO, NO, NO, BMS_F16_RMFD_OSB_1, BMS_F16_RMFD_OSB_2, BMS_F16_RMFD_OSB_3, BMS_F16_RMFD_OSB_4, BMS_F16_RMFD_OSB_5, NO, LY_BMS_F16_RMFD, BMS_F16_RMFD_OSB_6,
        BMS_F16_RMFD_OSB_19, BMS_F16_RMFD_INC_BRHT, BMS_F16_RMFD_INC_GAIN, NO, NO, NO, NO, NO, NO, NO, NO, BMS_F16_RMFD_OSB_7,
          BMS_F16_RMFD_OSB_18, BMS_F16_RMFD_DEC_BRHT, BMS_F16_RMFD_DEC_GAIN, BMS_F16_RMFD_OSB_15, BMS_F16_RMFD_OSB_14, BMS_F16_RMFD_OSB_13, BMS_F16_RMFD_OSB_12, BMS_F16_RMFD_OSB_11, NO, NO, BMS_F16_RMFD_OSB_8,
            BMS_F16_RMFD_OSB_17, BMS_F16_RMFD_OSB_16, NO, NO, NO, BMS_F16_RMFD_OSB_10, BMS_F16_RMFD_OSB_9),

    [LAYER_BMS_F16_PANELS] = UNIMAP_TANUKI(
        BMS_F16_MASTER_ARM_CYCLE, BMS_F16_MAN_PITCH_TOGGLE, NO, NO, NO, NO, NO, NO, BMS_F16_AVTR_TOGGLE, BMS_F16_SPOTLIGHT_TOGGLE, BMS_F16_NIGHTVISION_TOGGLE, BMS_F16_EJECT,
        BMS_F16_EMER_JETT_BTN, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, BMS_F16_MAIN_PWR_UP,
          BMS_F16_PRK_BRK_CYCLE, NO, NO, NO, NO, NO, NO, NO, NO, NO, BMS_F16_MAIN_PWR_DOWN,
             BMS_F16_LANDING_GEAR_TOGGLE, BMS_F16_WHEEL_BRAKES, NO,  NO,     NO,   BMS_F16_MRM_OVERRIDE_OR_PANELS_EN, NO),
};


HSV led_base_color = { .h = 192, .s = 192, .v = 0xff };


extern led_t led;

void hook_late_init(void)
{
    for (uint8_t i = 0; i < LED_COUNT; i++) {
        custom_led_set_color(i, led_base_color);
    }
    led.min_v = 0x40;
}


void hook_layer_change(uint32_t layer_state)
{
    int8_t active_layer = 0;
    uint8_t new_hue = 0;

    for (int8_t i = 31; i >= 0; i--) {
        if (layer_state & (1UL << i)) {
            active_layer = i;
            break;
        }
    }

    switch (active_layer) {
        case LAYER_BMS_F16_STICK:
            new_hue = 128; // blue
            break;
        case LAYER_BMS_F16_TQS:
            new_hue = 0; // red
            break;
        case LAYER_BMS_F16_ICP:
            new_hue = 78;  // green
            break;
        case LAYER_BMS_F16_LMFD:
        case LAYER_BMS_F16_RMFD:
            new_hue = 32;  // orange
            break;
        default:
            new_hue = led_base_color.h;
            break;
    }

    HSV new_color = {
        .h = new_hue,
        .s = led_base_color.s,
        .v = led_base_color.v
    };

#if LED_COUNT > 1
    HSV color0 = custom_led_get_color(0);
    HSV color1 = custom_led_get_color(1);
    if (color0.h != color1.h) {
        // in rainbow mode
        for (int8_t i = LED_COUNT - 1; i >= 0; i--) {
            /* To fix sudden brightness changes: */
            new_color.v = custom_led_get_color(i).v;
            custom_led_set_color(i, new_color);
            new_color.h = custom_led_rotate_val(new_color.h, -256 / LED_COUNT);
        }
    }
    else
    // in single-color mode
#endif
    for (uint8_t i = 0; i < LED_COUNT; i++) {
        /* To fix sudden brightness changes: */
        new_color.v = custom_led_get_color(i).v;
        custom_led_set_color(i, new_color);
    }
}


void hook_matrix_change(keyevent_t event)
{
    custom_led_hook_matrix_change(event);
}


void hook_keyboard_loop(void)
{
    custom_led_hook_keyboard_loop();
}


void hook_usb_suspend_entry(void)
{
    matrix_clear();
    clear_keyboard();
}


void hook_usb_wakeup(void)
{
    suspend_wakeup_init();
}


void action_reset(void)
{
    matrix_clear();
    clear_keyboard();

    // Flag for Caterina
    *(uint16_t *)0x0800 = 0x7777;
    bootloader_jump();
}


void action_function(keyrecord_t *record, uint8_t id, uint8_t opt)
{
    switch (id) {
        case FUNC_LED_SWITCH:
            action_led_switch(record);
            break;
        case FUNC_LED_ROTATE_HUE:
            action_led_rotate_hue(record);
            led_base_color = custom_led_get_color(LED_COUNT - 1);
            led_base_color.v = 0;
            break;
        case FUNC_LED_ROTATE_MIN_BRIGHTNESS:
            action_led_rotate_min_brightness(record);
            break;
        case FUNC_RESET:
            action_reset();
            break;
        default:
            break;
    }
}

