/*
Copyright 2012 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/io.h>
#include "stdint.h"
#include "led.h"
#include "action.h"
#include "action_layer.h"
#include "timer.h"

#include "custom_util.h"
#include "custom_led.h"
#include "ws2812.h"


led_t led = {
    .state = LED_ON,
    .min_v = 0x00,
    .max_v = 0xff,
    .activate_speed = 0x18,
    .fade_speed = 0x05,
    .update_interval = 25,
    .color_rotate_speed = 16,
};


static uint8_t key_to_led[MATRIX_ROWS][MATRIX_COLS] = {
    //Q  W  E  R  T  Y  U  I  O  P  BSPC
    { 4, 4, 3, 3, 2, 2, 1, 1, 1, 0, 0 },
    //A  S  D  F  G  H  J  K  L  SCLN ENTER
    { 4, 3, 3, 3, 2, 2, 1, 1, 1, 0,   0 },
    //LSFT Z  X  C  V  B  N  M  COMM DOT RSFT
    { 4,   4, 3, 3, 2, 2, 2, 1, 1,   0,  0 },
    //CAPS TAB LCTRL LGUI LALT SPC KANA N/A RALT RCTRL N/A
    { 4,   4,  4,    3,   3,   2,  2,   0,  1,   0,    0 },
};


void custom_led_set_color(uint8_t i, HSV c)
{
    if (i >= LED_COUNT) {
        return;
    }
    led.colors[i] = c;
}


HSV custom_led_get_color(uint8_t i)
{
    return led.colors[i];
}


uint8_t custom_led_rotate_val(uint8_t h, int8_t d)
{
    int16_t new_h = h + d;

    if (new_h >= 0) {
        while (new_h >= 256) {
            new_h -= 256;
        }
    } else {
        while (new_h < 0) {
            new_h += 256;
        }
    }

    return (uint8_t)new_h;
}


void custom_led_hook_matrix_change(keyevent_t event)
{
    if (led.state == LED_OFF) {
        return;
    }

    uint8_t led_id = key_to_led[event.key.row][event.key.col];
    int16_t new_v = led.colors[led_id].v + led.activate_speed;

    led.colors[led_id].v = new_v > led.max_v ? led.max_v : new_v;
}


void custom_led_hook_keyboard_loop(void)
{
    if (led.state == LED_OFF) {
        return;
    }

    static uint16_t last_led_update = 0;
    uint16_t cur_time = timer_read();
    int16_t new_v;

    if (TIMER_DIFF_16(cur_time, last_led_update) > led.update_interval) {
        RGB led_rgb_colors[LED_COUNT];
        for (uint8_t i = 0; i < LED_COUNT; i++) {
            new_v = led.colors[i].v - led.fade_speed;
            led.colors[i].v = new_v < led.min_v ? led.min_v : new_v;
            led_rgb_colors[i] = hsv_to_rgb(led.colors[i]);
        }

        ws2812_setleds(led_rgb_colors, LED_COUNT);

        last_led_update = cur_time;
    }
}


void action_led_switch(keyrecord_t *record)
{
    if (!record->event.pressed && KEY_TAPPED(record, 1)) {
        led.state = (led.state == LED_ON) ? LED_OFF : LED_ON;
        if (led.state == LED_OFF) {
            RGB led_off_colors[LED_COUNT];
            for (uint8_t i = 0; i < LED_COUNT; i++) {
                led_off_colors[i].r = 0;
                led_off_colors[i].g = 0;
                led_off_colors[i].b = 0;
            }
            ws2812_setleds(led_off_colors, LED_COUNT);
        }
    }
#if LED_COUNT > 1
    else if (record->event.pressed && KEY_TAPPED(record, 0)) {
        // Hold the switch key to switch LED modes
        HSV led_color = led.colors[LED_COUNT-1];

        if (led.colors[0].h != led.colors[1].h) {
            // switch to single-color mode
            for (uint8_t i = 0; i < LED_COUNT - 1; i++) {
                led.colors[i] = led_color;
            }
        } else {
            // switch to rainbow mode
            for (int8_t i = LED_COUNT - 2; i >= 0; i--) {
                led_color.h = custom_led_rotate_val(led_color.h, -256 / LED_COUNT);
                led.colors[i] = led_color;
            }
        }
    }
#endif
}


void action_led_rotate_hue(keyrecord_t *record)
{
    if (!record->event.pressed && !record->tap.interrupted) {
        for (uint8_t i = 0; i < LED_COUNT; i++) {
            uint8_t old_h = led.colors[i].h;
            led.colors[i].h = custom_led_rotate_val(old_h, led.color_rotate_speed);
        }
    }
}


void action_led_rotate_min_brightness(keyrecord_t *record)
{
    if (!record->event.pressed && !record -> tap.interrupted) {
        led.min_v = custom_led_rotate_val(led.min_v, led.color_rotate_speed);
    }
}


void led_set(uint8_t usb_led)
{
    if (usb_led & (1<<USB_LED_CAPS_LOCK)) {
    } else {
    }
}
