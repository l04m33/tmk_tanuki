/*
Copyright 2016 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef UNIMAP_TRANS_H
#define UNIMAP_TRANS_H

#include <stdint.h>
#include <stdbool.h>
#include "keycode.h"
#include "action.h"
#include "action_code.h"
#include "action_layer.h"
#include "action_macro.h"
#include "action_util.h"
#include "report.h"
#include "host.h"
#include "print.h"
#include "debug.h"
#include "unimap.h"

/* To distinguish the two SPACE keys, the KANA key slot is used
 * to map the right SPACE key.
 */

#define UNIMAP_TANUKI( \
    K31, K00, K01, K02, K03, K04, K05, K06, K07, K08, K09,  K0A, \
    K30,  K10, K11, K12, K13, K14, K15, K16, K17, K18, K19, K1A, \
       K20, K21, K22, K23, K24, K25, K26, K27, K28, K29, K2A, \
        K32, K33, K34,       K35,      K36,      K38, K39  \
) UNIMAP( \
            NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO,                                     \
    NO,     NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO,       NO, NO, NO,       NO, NO, NO, \
    NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, NO, K0A,  NO, NO, NO,   NO, NO, NO, NO, \
    K31,K00,K01,K02,K03,K04,K05,K06,K07,K08,K09,NO, NO,     NO,   NO, NO, NO,   NO, NO, NO, NO, \
    K30,K10,K11,K12,K13,K14,K15,K16,K17,K18,K19,NO,     NO, K1A,                NO, NO, NO, NO, \
    K20,NO, K21,K22,K23,K24,K25,K26,K27,K28,K29,NO, NO,     K2A,      NO,       NO, NO, NO, NO, \
    K32,K33,K34,NO,         K35,        NO, K36,K38,NO, NO, K39,  NO, NO, NO,   NO,     NO, NO  \
)

/* Mapping to Universal keyboard layout
 *
 * Universal keyboard layout
 *         ,-----------------------------------------------.
 *         |F13|F14|F15|F16|F17|F18|F19|F20|F21|F22|F23|F24|
 * ,---.   |-----------------------------------------------|     ,-----------.     ,-----------.
 * |Esc|   |F1 |F2 |F3 |F4 |F5 |F6 |F7 |F8 |F9 |F10|F11|F12|     |PrS|ScL|Pau|     |VDn|VUp|Mut|
 * `---'   `-----------------------------------------------'     `-----------'     `-----------'
 * ,-----------------------------------------------------------. ,-----------. ,---------------.
 * |  `|  1|  2|  3|  4|  5|  6|  7|  8|  9|  0|  -|  =|JPY|Bsp| |Ins|Hom|PgU| |NmL|  /|  *|  -|
 * |-----------------------------------------------------------| |-----------| |---------------|
 * |Tab  |  Q|  W|  E|  R|  T|  Y|  U|  I|  O|  P|  [|  ]|  \  | |Del|End|PgD| |  7|  8|  9|  +|
 * |-----------------------------------------------------------| `-----------' |---------------|
 * |CapsL |  A|  S|  D|  F|  G|  H|  J|  K|  L|  ;|  '|  #|Retn|               |  4|  5|  6|KP,|
 * |-----------------------------------------------------------|     ,---.     |---------------|
 * |Shft|  <|  Z|  X|  C|  V|  B|  N|  M|  ,|  .|  /| RO|Shift |     |Up |     |  1|  2|  3|KP=|
 * |-----------------------------------------------------------| ,-----------. |---------------|
 * |Ctl|Gui|Alt|MHEN|     Space      |HENK|KANA|Alt|Gui|App|Ctl| |Lef|Dow|Rig| |  0    |  .|Ent|
 * `-----------------------------------------------------------' `-----------' `---------------'
 */

/* Tanuki
 * ,-------------------------------------------------------.
 * |Tab  |  Q|  W|  E|  R|  T|  Y|  U|  I|  O|  P|     BSPC|
 * |-------------------------------------------------------|
 * |CapsL |  A|  S|  D|  F|  G|  H|  J|  K|  L|  ;|   Enter|
 * `-------------------------------------------------------'
 *    |Shift |  Z|  X|  C|  V|  B|  N|  M|  ,|  .| Shift|
 *    `-------------------------------------------------'
 *      |Ctrl |  Gui| Alt|  Space |  KANA | Alt| Ctrl|
 *      `--------------------------------------------'
 *
 * Matrix - row:4 x col:11
 * ,-------------------------------------------------------.
 * | 31  | 00| 01| 02| 03| 04| 05| 06| 07| 08| 09|      0A |
 * |-------------------------------------------------------|
 * | 30   | 10| 11| 12| 13| 14| 15| 16| 17| 18| 19|     1A |
 * `-------------------------------------------------------'
 *    | 20   | 21| 22| 23| 24| 25| 26| 27| 28| 29|   2A |
 *    `-------------------------------------------------'
 *      | 32  |  33 | 34 |    35  |   36  | 38 |  39 |
 *      `--------------------------------------------'
 */

const uint8_t PROGMEM unimap_trans[MATRIX_ROWS][MATRIX_COLS] = {
    { UNIMAP_Q,        UNIMAP_W,   UNIMAP_E,     UNIMAP_R,    UNIMAP_T,    UNIMAP_Y,     UNIMAP_U,    UNIMAP_I,  UNIMAP_O,     UNIMAP_P,      UNIMAP_BSPACE },
    { UNIMAP_A,        UNIMAP_S,   UNIMAP_D,     UNIMAP_F,    UNIMAP_G,    UNIMAP_H,     UNIMAP_J,    UNIMAP_K,  UNIMAP_L,     UNIMAP_SCOLON, UNIMAP_ENTER  },
    { UNIMAP_LSHIFT,   UNIMAP_Z,   UNIMAP_X,     UNIMAP_C,    UNIMAP_V,    UNIMAP_B,     UNIMAP_N,    UNIMAP_M,  UNIMAP_COMMA, UNIMAP_DOT,    UNIMAP_RSHIFT },
    { UNIMAP_CAPSLOCK, UNIMAP_TAB, UNIMAP_LCTRL, UNIMAP_LGUI, UNIMAP_LALT, UNIMAP_SPACE, UNIMAP_KANA, UNIMAP_NO, UNIMAP_RALT,  UNIMAP_RCTRL,  UNIMAP_NO },
};

#endif
