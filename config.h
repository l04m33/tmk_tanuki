/*
Copyright 2012 Jun Wako <wakojun@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONFIG_H
#define CONFIG_H


/* USB Device descriptor parameter */
#define VENDOR_ID       0x4B1D
#define PRODUCT_ID      0x0074
#define DEVICE_VER      0x0003
#define MANUFACTURER    If you found this please contact l04m33@gmail.com
#define PRODUCT         Tanuki
#define DESCRIPTION     Custom firmware for Tanuki

/* key matrix size */
#define MATRIX_ROWS 4
#define MATRIX_COLS 11

/* define if matrix has ghost */
//#define MATRIX_HAS_GHOST

/* number of backlight levels */
#define BACKLIGHT_LEVELS 1

/* Set 0 if debouncing isn't needed */
#define DEBOUNCE    6
#define TAPPING_TERM    175

/* Mechanical locking support. Use KC_LCAP, KC_LNUM or KC_LSCR instead in keymap */
#define LOCKING_SUPPORT_ENABLE
/* Locking resynchronize hack */
#define LOCKING_RESYNC_ENABLE

/* key combination for command */
#define IS_COMMAND() ( \
    keyboard_report->mods == (MOD_BIT(KC_LSHIFT) | MOD_BIT(KC_RSHIFT)) \
)



/*
 * Feature disable options
 *  These options are also useful to firmware size reduction.
 */

/* disable debug print */
/*
 * Firmware was too large with Penti mode and d_macro all enabled.
 * Disable debug messages to fit it in.
 */
#define NO_DEBUG

/* disable print */
//#define NO_PRINT

/* disable action features */
//#define NO_ACTION_LAYER
//#define NO_ACTION_TAPPING
//#define NO_ACTION_ONESHOT
//#define NO_ACTION_MACRO
//#define NO_ACTION_FUNCTION

//#define NO_PENTI
//#define NO_D_MACRO
//#define NO_SHIFT_PAREN

// The LEDs are controlled by PIN D1 
#define RGB_DI_PIN ((0x9 << 4) | 1)

#endif
