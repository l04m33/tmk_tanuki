#ifndef custom_led_h_INCLUDED
#define custom_led_h_INCLUDED

#include "action.h"
#include "color.h"
#include "ws2812.h"

#define LED_COUNT 5

typedef enum {
    LED_OFF = 0,
    LED_ON,
} led_state_t;

typedef struct {
    led_state_t state;
    HSV colors[LED_COUNT];
    uint8_t min_v;
    uint8_t max_v;
    uint8_t activate_speed;
    uint8_t fade_speed;
    uint16_t update_interval;
    int8_t color_rotate_speed;
} led_t;

extern led_t led;
void custom_led_set_color(uint8_t i, HSV c);
HSV  custom_led_get_color(uint8_t i);
uint8_t custom_led_rotate_val(uint8_t h, int8_t d);
void custom_led_hook_matrix_change(keyevent_t event);
void custom_led_hook_keyboard_loop(void);
void action_led_switch(keyrecord_t *record);
void action_led_rotate_hue(keyrecord_t *record);
void action_led_rotate_min_brightness(keyrecord_t *record);

#endif // custom_led_h_INCLUDED

