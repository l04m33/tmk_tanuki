#ifndef custom_util_h_INCLUDED
#define custom_util_h_INCLUDED

#ifndef KEY_TAPPED
#define KEY_TAPPED(_rec_, _count_) ((_rec_)->tap.count == (_count_) && !(_rec_)->tap.interrupted)
#endif

#endif // custom_util_h_INCLUDED

