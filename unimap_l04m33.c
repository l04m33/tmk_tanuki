#include "keymap.h"
#include "keyboard.h"
#include "action.h"
#include "matrix.h"
#include "suspend.h"
#include "bootloader.h"
#include "unimap_trans.h"
#if defined(__AVR__)
#   include <avr/pgmspace.h>
#endif

#include "custom_util.h"
#ifndef NO_SHIFT_PAREN
  #include "shift_paren.h"
#endif
#ifndef NO_D_MACRO
  #include "d_macro.h"
#endif
#ifndef NO_PENTI
  #include "penti.h"
#endif
#include "custom_led.h"


enum function_id {
#ifndef NO_SHIFT_PAREN
    FUNC_LSHIFT_LPAREN,
    FUNC_RSHIFT_RPAREN,
    FUNC_AUTO_LBRACKET,
    FUNC_AUTO_RBRACKET,
    FUNC_AUTO_PAREN,
#endif
#ifndef NO_D_MACRO
    FUNC_D_MACRO_RECORD,
    FUNC_D_MACRO_PLAY,
    FUNC_D_MACRO_PLAY_1,
    FUNC_D_MACRO_PLAY_2,
#endif
#ifndef NO_PENTI
    FUNC_PENTI_KEY,
#endif
    FUNC_LED_SWITCH,
    FUNC_LED_ROTATE_HUE,
    FUNC_LED_ROTATE_MIN_BRIGHTNESS,
    FUNC_RESET,
};

enum layer_id {
    LAYER_COLEMAK_DH = 0,
    LAYER_QWERTY,
    LAYER_SPCFN,
    LAYER_SWITCHES,
    LAYER_NP,
#ifndef NO_PENTI
    LAYER_PENTI,
#endif

    LAYER_COUNT,
};

#define AC_CTLESC      ACTION_MODS_TAP_KEY(MOD_LCTL, KC_ESC)
#define AC_CTLENT      ACTION_MODS_TAP_KEY(MOD_RCTL, KC_ENT)
#define AC_ALTBSP      ACTION_MODS_TAP_KEY(MOD_LALT, KC_BSPC)
#define AC_SPCFN       ACTION_LAYER_TAP_KEY(LAYER_SPCFN, KC_SPC)
#define AC_TABNP       ACTION_LAYER_TAP_KEY(LAYER_NP, KC_TAB)

#define AC_SWTS        ACTION_LAYER_TAP_TOGGLE(LAYER_SWITCHES)
#define AC_QWERTY      ACTION_LAYER_TOGGLE(1)
#define AC_SPCFNT      ACTION_LAYER_TOGGLE(LAYER_SPCFN)

#ifndef NO_SHIFT_PAREN
  #define AC_LSFTPRN     ACTION_FUNCTION_TAP(FUNC_LSHIFT_LPAREN)
  #define AC_RSFTPRN     ACTION_FUNCTION_TAP(FUNC_RSHIFT_RPAREN)
  #define AC_AUTOLBRC    ACTION_FUNCTION_TAP(FUNC_AUTO_LBRACKET)
  #define AC_AUTORBRC    ACTION_FUNCTION_TAP(FUNC_AUTO_RBRACKET)
  #define AC_AUTOPRN     ACTION_FUNCTION_TAP(FUNC_AUTO_PAREN)
#else
  #define AC_LSFTPRN     AC_LSFT
  #define AC_RSFTPRN     AC_RSFT
  #define AC_AUTOLBRC    AC_LBRC
  #define AC_AUTORBRC    AC_RBRC
  #define AC_AUTOPRN     AC_TRNS
#endif

#ifndef NO_D_MACRO
  #define AC_MREC      ACTION_FUNCTION_TAP(FUNC_D_MACRO_RECORD)
  #define AC_MPLAY     ACTION_FUNCTION_TAP(FUNC_D_MACRO_PLAY)
  #define AC_MPLAY1    ACTION_FUNCTION_TAP(FUNC_D_MACRO_PLAY_1)
  #define AC_MPLAY2    ACTION_FUNCTION_TAP(FUNC_D_MACRO_PLAY_2)
#else
  #define AC_MREC      AC_TRNS
  #define AC_MPLAY     AC_TRNS
  #define AC_MPLAY1    AC_TRNS
  #define AC_MPLAY2    AC_TRNS
#endif

#ifndef NO_PENTI
  #define AC_PENTI        ACTION_LAYER_TOGGLE(LAYER_PENTI)
  #define AC_PENTI_THUMB  ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_THUMB_BIT)
  #define AC_PENTI_INDEX  ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_INDEX_BIT)
  #define AC_PENTI_MIDDLE ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_MIDDLE_BIT)
  #define AC_PENTI_RING   ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_RING_BIT)
  #define AC_PENTI_PINKY  ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_PINKY_BIT)
  #define AC_PENTI_REPEAT ACTION_FUNCTION_OPT(FUNC_PENTI_KEY, PENTI_REPEAT_BIT)
#else
  #define AC_PENTI AC_TRNS
#endif

#define AC_LEDSW       ACTION_FUNCTION_TAP(FUNC_LED_SWITCH)
#define AC_LEDHUE      ACTION_FUNCTION_TAP(FUNC_LED_ROTATE_HUE)
#define AC_LEDBRT      ACTION_FUNCTION_TAP(FUNC_LED_ROTATE_MIN_BRIGHTNESS)

#define AC_RESET       ACTION_FUNCTION_TAP(FUNC_RESET)

#ifndef _
#   define _ TRNS
#else
#   error "The _ macro is already defined!"
#endif

#ifdef KEYMAP_SECTION_ENABLE
const action_t actionmaps[][UNIMAP_ROWS][UNIMAP_COLS] __attribute__ ((section (".keymap.keymaps"))) = {
#else
const action_t actionmaps[][UNIMAP_ROWS][UNIMAP_COLS] PROGMEM = {
#endif

    [LAYER_COLEMAK_DH] = UNIMAP_TANUKI(
        TABNP,    Q, W, F, P, B, J, L, U,   Y,   SCLN, AUTOLBRC,
        CTLESC,   A, R, S, T, G, M, N, E,   I,   O,    CTLENT,
          LSFTPRN, X, C, D, V, Z, K, H, COMM, DOT, RSFTPRN,
             SWTS,LGUI, ALTBSP, SPCFN, SPCFN, RALT,RGUI),

    [LAYER_QWERTY] = UNIMAP_TANUKI(
        _, Q, W, E, R, T, Y, U, I, O, P,    _,
        _, A, S, D, F, G, H, J, K, L, SCLN, _,
         _, Z, X, C, V, B, N, M, _, _,    _,
          _, _, _,     _,     _,     _, _),

    [LAYER_SPCFN] = UNIMAP_TANUKI(
        GRV,  EQL, 3, 2, 1,   MINS, HOME, PGDN, PGUP, END,  DELETE, LBRC,
        LCTL, 0,   6, 5, 4,   QUOT, LEFT, DOWN, UP,   RGHT, INS,    RCTL,
          LSFT, 9,   8, 7, DOT, ENT,  RBRC, BSLS, _,    _,    SLSH,
           _,    _,   _,           _,          _,        _,    _),

    [LAYER_NP] = UNIMAP_TANUKI(
        TABNP, PPLS, P3, P2, P1, PMNS, NLCK, NO, NO, NO, NO, NO,
        _,     P0,   P6, P5, P4, PSLS, PAST, NO, NO, NO, NO, _,
          _,     P9, P8, P7, PDOT, PENT, NO, NO, NO, NO,  _,
           _,      _,   _,        _,       _,     _,   _),

    [LAYER_SWITCHES] = UNIMAP_TANUKI(
        PENTI, F11, F3, F2, F1, PSCR, QWERTY, SPCFNT, AUTOPRN, LEDSW,  LEDHUE, LEDBRT,
        _,     F10, F6, F5, F4, CAPS, MS_L,   MS_D,   MS_U,    MS_R,   BTN1,   BTN2,
           _,    F9, F8, F7, F12, SLCK, MREC,   MPLAY,  MPLAY1,  MPLAY2,   _,
               _,   _,   _,       RESET,           _,           _,   _),

#ifndef NO_PENTI
    [LAYER_PENTI] = UNIMAP_TANUKI(
        PENTI, PENTI_PINKY, PENTI_RING,   PENTI_MIDDLE, NO,          NO, NO, NO,          PENTI_MIDDLE, PENTI_RING, PENTI_PINKY, NO,
        NO,    NO,          PENTI_REPEAT, NO,           PENTI_INDEX, NO, NO, PENTI_INDEX, PENTI_REPEAT, NO,         NO,          NO,
          NO,    NO,          NO,         NO,           NO,          NO, NO, NO,          NO,           NO,      NO,
            _,     NO,          PENTI_THUMB,        NO,          PENTI_THUMB,              NO,   NO),
#endif
};


HSV led_base_color = { .h = 192, .s = 192, .v = 0 };


void hook_late_init(void)
{
#ifndef NO_SHIFT_PAREN
    auto_paren_state.enabled = 1;
#endif
#ifndef NO_D_MACRO
    d_macro_stack_scan_hook_late_init();
#endif

    for (uint8_t i = 0; i < LED_COUNT; i++) {
        custom_led_set_color(i, led_base_color);
    }
}


void hook_layer_change(uint32_t layer_state)
{
    int8_t active_layer = 0;

    for (int8_t i = 31; i >= 0; i--) {
        if (layer_state & (1UL << i)) {
            active_layer = i;
            break;
        }
    }

    HSV new_color = {
        .h = custom_led_rotate_val(led_base_color.h, 256 / LAYER_COUNT * active_layer),
        .s = led_base_color.s,
        .v = led_base_color.v
    };

#if LED_COUNT > 1
    HSV color0 = custom_led_get_color(0);
    HSV color1 = custom_led_get_color(1);
    if (color0.h != color1.h) {
        // in rainbow mode
        for (int8_t i = LED_COUNT - 1; i >= 0; i--) {
            /* To fix sudden brightness changes: */
            new_color.v = custom_led_get_color(i).v;
            custom_led_set_color(i, new_color);
            new_color.h = custom_led_rotate_val(new_color.h, -256 / LED_COUNT);
        }
    }
    else
    // in single-color mode
#endif
    for (uint8_t i = 0; i < LED_COUNT; i++) {
        /* To fix sudden brightness changes: */
        new_color.v = custom_led_get_color(i).v;
        custom_led_set_color(i, new_color);
    }
}


void hook_matrix_change(keyevent_t event)
{
#ifndef NO_D_MACRO
    d_macro_hook_matrix_change(event);
#endif
    custom_led_hook_matrix_change(event);
}


void hook_keyboard_loop(void)
{
#ifndef NO_D_MACRO
    d_macro_stack_scan();
    d_macro_hook_keyboard_loop();
#endif
    custom_led_hook_keyboard_loop();
}


void hook_usb_suspend_entry(void)
{
    matrix_clear();
    clear_keyboard();
}


void hook_usb_wakeup(void)
{
    suspend_wakeup_init();
}


void action_reset(void)
{
    matrix_clear();
    clear_keyboard();

    // Flag for Caterina
    *(uint16_t *)0x0800 = 0x7777;
    bootloader_jump();
}


void action_function(keyrecord_t *record, uint8_t id, uint8_t opt)
{
    switch (id) {
#ifndef NO_SHIFT_PAREN
        case FUNC_LSHIFT_LPAREN:
            action_shift_paren(record, KC_LSHIFT);
            break;
        case FUNC_RSHIFT_RPAREN:
            action_shift_paren(record, KC_RSHIFT);
            break;
        case FUNC_AUTO_LBRACKET:
            action_shift_paren(record, KC_LBRACKET);
            break;
        case FUNC_AUTO_RBRACKET:
            action_shift_paren(record, KC_RBRACKET);
            break;
        case FUNC_AUTO_PAREN:
            action_auto_paren(record);
            break;
#endif
#ifndef NO_D_MACRO
        case FUNC_D_MACRO_RECORD:
            d_macro_action_record(record);
            break;
        case FUNC_D_MACRO_PLAY:
            d_macro_action_play(record, -1);
            break;
        case FUNC_D_MACRO_PLAY_1:
            d_macro_action_play(record, 1);
            break;
        case FUNC_D_MACRO_PLAY_2:
            d_macro_action_play(record, 50);
            break;
#endif
#ifndef NO_PENTI
        case FUNC_PENTI_KEY:
            action_penti_key(record, opt);
            break;
#endif
        case FUNC_LED_SWITCH:
            action_led_switch(record);
            break;
        case FUNC_LED_ROTATE_HUE:
            action_led_rotate_hue(record);
            led_base_color = custom_led_get_color(LED_COUNT - 1);
            led_base_color.v = 0;
            break;
        case FUNC_LED_ROTATE_MIN_BRIGHTNESS:
            action_led_rotate_min_brightness(record);
            break;
        case FUNC_RESET:
            action_reset();
            break;
        default:
            break;
    }
}


extern keypos_t unimap_translate(keypos_t key);

action_t action_for_key(uint8_t layer, keypos_t key)
{
    keypos_t uni = unimap_translate(key);
    if ((uni.row << 4 | uni.col) == UNIMAP_NO) {
        return (action_t)ACTION_NO;
    }

    action_t action =
#if defined(__AVR__)
        (action_t)pgm_read_word(&actionmaps[(layer)][(uni.row & 0x7)][(uni.col)]);
#else
        actionmaps[(layer)][(uni.row & 0x7)][(uni.col)];
#endif

#ifndef NO_D_MACRO
    return d_macro_action_for_key(key, action);
#else
    return action;
#endif
}
