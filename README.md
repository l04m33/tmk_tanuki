# TMK Firmware for the Tanuki 40% Keyboard #

This is the firmware for my own [Tanuki](https://github.com/SethSenpai/Tanuki).

The LED driver and color related codes are ripped from the QMK
firmware.

## License ##

GPLv2+
